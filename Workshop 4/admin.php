<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <title>Document</title>
</head>
<body>
    <a class="nav-link disabled" href="close_session.php" tabindex="-1" aria-disabled="true">Logout</a>
    <h1>Administrador</h1>
    <form action="nuevo_cliente.php" method="POST" class="form-inline" role="form">
        <div class="form-group">
            <label class="sr-only" for="">Username</label>
            <input type="text" class="form-control" name="username" placeholder="Username">
        </div>
        <div class="form-group">
            <label class="sr-only" for="">Password</label>
            <input type="password" class="form-control" name="password" placeholder="Password">
        </div>
        <div class="form-group">
            <label class="sr-only" for="">Name</label>
            <input type="text" class="form-control" name="name" placeholder="Name">
        </div>
        <div class="form-group">
            <label class="sr-only" for="">Last name</label>
            <input type="text" class="form-control" name="last_name" placeholder="Last name">
        </div>
        <input type="submit" class="btn btn-primary" value="Submit"></input>
    </form>
</body>
</html>