<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{

  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function authenticate($username, $password)
  {
    $query = $this->db->get_where('user', array('username' => $username, 'password' => $password));
    if ($query->result()) {
      return $query->result()[0];
    } else {
      return false;
    }
  }
  /**
   *  Inserts a new user in the database
   *
   * @param $user  An associative array with all user data
   */
  public function insert($user)
  {
    $query = $this->db->insert('user', $user);

    if ($this->db->affected_rows() > 0) {
      return true;
    } else {
      return false;
    }
  }

  public function getUser($id)
  {
    $query = $this->db->get_where('user', array('id' => $id));
    if ($query->result()) {
      return $query->result()[0];
    } else {
      return false;
    }
  }

  public function LoadUser($id)
  {
    $query = $this->db->get_where('user', array('id' => $id));
    return $query->result();
  }

  function update($id, $name, $lastname, $username, $password)
  {
    $this->db->where('id', $id);
    $this->db->set('name', $name);
    $this->db->set('lastname', $lastname);
    $this->db->set('username', $username);
    $this->db->set('password', $password);
    return $this->db->update('user');
  }
  
  /**
   *  Validate in the database that the user exists
   *
   * @param $username  The username
   * @param $password The user's password
   */
  public function getByName($name)
  {
    $query = $this->db->get_where('user', array('name' => $name));
    if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }
  }
  public function delete($id)
  {
    $this->db->delete('user', array('id' => $id));
  }

  /**
   *  Get user by Id
   *
   * @param $id  The user's id
   */
  public function getById($id)
  {
    $query = $this->db->get_where('user', array('id' => $id));
    if ($query->result()) {
      return $query->result();
    } else {
      return false;
    }
  }

  /**
   *  Get all users from the database
   *
   */
  public function all()
  {
    $query = $this->db->get('user');
    return $query->result();
  }
}
